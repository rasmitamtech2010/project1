package testcase;
import wdMethods.ProjectMethods;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.*;

public class TC001_LoginAndLogOut extends ProjectMethods{
	
	@BeforeTest
	public void beforeTest()
	{
		testCaseName="TC001_Login";
		testCaseDesc="To Create A lead";
		author="Abi";
		category="Smoke";
		System.out.println("before test");
	}
	@Test
	public void login(){
		try {
			startApp("chrome", "http://leaftaps.com/opentaps");
			WebElement eleUserName = locateElement("id", "username");
			type(eleUserName, "DemoSalesManager");
			WebElement elePassword = locateElement("password");
			type(elePassword, "crmsfa");
			WebElement login = locateElement("classname","decorativeSubmit");
			click(login);
			Thread.sleep(3000);
			WebElement eleLogout = locateElement("classname", "decorativeSubmit");
			click(eleLogout);
		} catch (IOException e) {
			System.err.println("cannot login");
		}
		
		
	}
	static void testcasename()
	{
		testCaseName="TC001_Login";
	}
	
}







