package testcase;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class TC005_DeleteLead extends ProjectMethods{
	@BeforeTest(groups="sanity")
	public void beforeTest()
	{
		testCaseName="TC005_Delete Lead";
		testCaseDesc="To Delete lead";
		author="Abi";
		category="Smoke";
		System.out.println("before test");
	}
	@Test(groups="sanity")
	public void deleteLead() throws InterruptedException, IOException
	{
		
		Thread.sleep(3000);
		click(locateElement("linktext", "Leads"));
		Thread.sleep(3000);

		//Delete lead
		click(locateElement("linktext", "Find Leads"));
		Thread.sleep(3000);
		
		click(locateElement("xpath","//span[text()='Phone']"));
		Thread.sleep(2000);
		
		type(locateElement("xpath","//input[@name='phoneNumber']"),"9843");
		
		click(locateElement("xpath","//button[text()='Find Leads']"));
		Thread.sleep(10000);
		WebElement delete = locateElement("xpath","(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a");
		String del = delete.getText();
		click(delete);
		Thread.sleep(3000);
		click(locateElement("linktext", "Delete"));
		Thread.sleep(3000);
		click(locateElement("linktext", "Find Leads"));
		
		type(locateElement("xpath","//input[@name='id']"),del);
		
		click(locateElement("xpath","//button[text()='Find Leads']"));

	}
	
}
