package week4.hw;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class TestCasesCreateLead {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		
		//Creating a obj for chrome driver
		ChromeDriver driver=new ChromeDriver();
		try {
		//Loading URL
		driver.get("http://leaftaps.com/opentaps/control/main");
		
		//Implicit wait
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				
		//Maximize the window
		driver.manage().window().maximize();
		
		//Entering user name 

		try {
			driver.findElementById("username").sendKeys("DemoSalesManager");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
				
		//Enter Password
		driver.findElementById("password").sendKeys("crmsfa");
		
		//Click Login
		driver.findElementByClassName("decorativeSubmit").click();
		
		//Click CRM/SFA link
		driver.findElementByLinkText("CRM/SFA").click();
		
		//Click leads and enter the details
		driver.findElementByLinkText("Create Lead").click();
		
		driver.findElementByLinkText("Create Lead").click();
		driver.findElementById("createLeadForm_companyName").sendKeys("Infosys");
		String n1=driver.findElementById("createLeadForm_companyName").getAttribute("maxlength");
		System.out.println(n1);
		String n2=driver.findElementById("createLeadForm_companyName").getAttribute("name");
		System.out.println(n2);
		driver.findElementById("createLeadForm_firstName").sendKeys("Rasmita");
		driver.findElementById("createLeadForm_firstName").sendKeys("Rasmi ");
		driver.findElementById("createLeadForm_lastName").sendKeys("Satapathy");
		driver.findElementById("createLeadForm_primaryPhoneCountryCode").clear();
		driver.findElementById("createLeadForm_personalTitle").sendKeys("Welcome!!!");
		driver.findElementById("createLeadForm_birthDate").sendKeys("05/07/1989");
		driver.findElementById("createLeadForm_generalProfTitle").sendKeys("Test Lead");
		driver.findElementById("createLeadForm_departmentName").sendKeys("Cosmos");
		driver.findElementById("createLeadForm_annualRevenue").sendKeys("100000");
		driver.findElementById("createLeadForm_annualRevenue").sendKeys("100000");
		driver.findElementById("createLeadForm_numberEmployees").sendKeys("50000");
		driver.findElementById("createLeadForm_sicCode").sendKeys("1007");
		driver.findElementById("createLeadForm_tickerSymbol").sendKeys("@Q");
		
	
		
		
		WebElement sc=driver.findElementById("createLeadForm_currencyUomId");
		Select sl=new Select(sc);
		sl.selectByVisibleText("USD - American Dollar");
		
		WebElement sc1=driver.findElementById("createLeadForm_industryEnumId");
		Select sl1=new Select(sc1);
		sl1.selectByVisibleText("Computer Software");
		
		WebElement sc11=driver.findElementById("createLeadForm_ownershipEnumId");
		Select sl11=new Select(sc11);
		sl11.selectByVisibleText("Public Corporation");
		
		
	
		
		
		
			
		
		//For drop down box -->Find the drop down and store it in a web element
		WebElement src=driver.findElementById("createLeadForm_dataSourceId");
		//Create obj for select class to select a options form Drop down
		Select slct=new Select(src);
		//By using select class obj select the value with visible text
		slct.selectByVisibleText("Direct Mail");
		
		WebElement MC=driver.findElementById("createLeadForm_marketingCampaignId");
		Select slct1=new Select(MC);
		//Get all the options in a drop down and store it in a list
		List<WebElement> lMc=slct1.getOptions();//get options method return type is "List"
		int size=lMc.size();
		slct1.selectByIndex(size-1);
		driver.findElementByClassName("smallSubmit").click();
		
		
		
		
		
		
		
		
		
		
		}catch (NoSuchElementException e)
		{e.printStackTrace();
		System.out.println("No such Element exception");
		}finally{
		driver.close();
	}
	}
	

}
