package week4.hw;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

public class MergeLead {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
				// To open a chrome browser
				System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
				
				//Creating a obj for chrome driver
				ChromeDriver driver=new ChromeDriver();
				
				//Loading URL
				driver.get("http://leaftaps.com/opentaps/control/main");
				
				//Implicit wait
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
						
				//Maximize the window
				driver.manage().window().maximize();
				
				//Entering user name 
				driver.findElementById("username").sendKeys("DemoSalesManager");
				
				//Enter Password
				driver.findElementById("password").sendKeys("crmsfa");
				
				//Click Login
				driver.findElementByClassName("decorativeSubmit").click();
				
				//Click CRM/SFA link
				driver.findElementByLinkText("CRM/SFA").click();
				
				
				//lead
				driver.findElementByLinkText("Leads").click();
				
				
				//Click leads and enter the details
				//driver.findElementByLinkText("Create Lead").click();
				
				//Click merge leads
				driver.findElementByLinkText("Merge Leads").click();
				
				//Click on Icon Near from lead and move to new window
				driver.findElementByXPath("(//img[@alt='Lookup'])[1]").click();

			
				//Enter lead Id
				//driver.findElementById("//div[@id='x-form-el-ext-gen25']//input").sendKeys("10200");
				
				
				//click on find lead button
				//driver.findElementByXPath("//button[text()='Find Leads']").click();
				
				//click on first resulting lead
				//driver.findElementByXPath("//div[@class='x-grid3-cell-inner x-grid3-col-partyId'][1]/a").click();
				
				//Click merge
				driver.findElementByClassName("buttonDangerous").click();
				
				//Accept Alert
				
				
				//click on find lead button
				driver.findElementByXPath("//button[text()='Find Leads']").click();
				
				//enter from lead id
				
				
				//click find leads
				
				
				
				//verify error message
				
				
				
				//close the browser(do not log out)
				driver.close();
				
	
	
	}

}
