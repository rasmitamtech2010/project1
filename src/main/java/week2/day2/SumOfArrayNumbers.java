package week2.day2;

import java.util.Scanner;

public class SumOfArrayNumbers {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
        int n, sum = 0;
        Scanner scr = new Scanner(System.in);
        System.out.print("Enter the size of the array :");
        n = scr.nextInt();
        int a[] = new int[n];
        System.out.println("Enter all the elements:");
        for(int i = 0; i < n; i++)
        {
            a[i] = scr.nextInt();
            sum = sum + a[i];
        }
        System.out.println("Sum of numbers in an array : "+sum);
    }

	}
