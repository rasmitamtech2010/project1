package week2.day2;

import java.util.Scanner;

public class DisplayPrimeNumberUptoN {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter the value of N: ");
	    int N = scan.nextInt();
	    int x = 2;
	   
	    for(int i = 0; i <= N; i++)
	    {
	        int count = 0;

	        for(int j = 1; j <= x; j++)
	            if(x%j == 0)
	                count++;

	        if(count == 2)
	            System.out.print(x + ",");

	        x++;
	    }
	}

}
