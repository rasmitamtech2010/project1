package week2.day2;

import java.util.Scanner;

public class ArithOP {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scr=new Scanner(System.in);
		System.out.println("Enter The First Number: ");
		int N1=scr.nextInt();
		System.out.println("Enter The Second Number: ");
		int N2=scr.nextInt();
		System.out.println("Enter your Choice : ");
		System.out.println("Choose The Operation from Add/Substract/Multiply/Divide : ");
		//char operator = scr.next().charAt(0);
		String s=scr.next();
        int result = 0;
        switch(s)
        {
            case "Add":
            	result = N1 + N2;
                break;

            case "Substract":
            	result = N1 - N2;
                break;

            case "Multiply":
            	result = N1 * N2;
                break;

            case "Divide":
            	result = N1 / N2;
                break;

            default:
                System.out.printf("You have entered wrong operation!!!!");
                break;
        }

        System.out.println(N1+" "+s+" "+N2+": "+result);
    }

}




