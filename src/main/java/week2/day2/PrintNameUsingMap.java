package week2.day2;

import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

public class PrintNameUsingMap {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Set<String> names=new LinkedHashSet<String>();
		String name="Rasmita Satapathy";
		char[] ch=name.toCharArray();
		Map<Character,Integer> map=new HashMap<Character,Integer>();
		for(char c:ch) {
			if(map.containsKey(c)) {
				//System.out.println(c);
				Integer val=map.get(c)+1;
				map.put(c,val);
				System.out.println("Duplicate Char is "+c);
				}else
				{
					map.put(c,1);
				}
			
		}
	}

}
