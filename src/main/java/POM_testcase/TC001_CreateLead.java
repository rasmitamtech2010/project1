package POM_testcase;
import wdMethods.ProjectMethods;

import java.io.IOException;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import POM_pages.MyHomePage;
import wdMethods.ProjectMethods;



public class TC001_CreateLead extends ProjectMethods
{
	@BeforeTest(groups = {"common"})
	public void setValues()
	{
		testCaseName = "TC001";
		testCaseDesc = "Create Lead";
		category = "smoke";		
		ExcelFileName = "CreateLead";
		author = "Sumathy";
	}
	
	@Test(invocationCount =1 , dataProvider="positive")
	public void TestCase(String company, String fName, String lName, String email, String phoneNumber) throws IOException 
	{	
		new MyHomePage()
		.clickLeadsLink()
		.clickCreateLeadLink()
		.enterCompanyName(company)
		.enterFName(fName)
		.enterLName(lName)
		.enterEmail(email)
		.enterPhoneNumber(phoneNumber)
		.clickCreateLead()
		.verifyFName(fName);
	
	}
	}
		

