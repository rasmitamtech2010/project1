package week1.day2;

import java.util.Arrays;
import java.util.Scanner;

public class secSmallestThree {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner s = new Scanner(System.in);
        System.out.print("Enter number of elements in the array:");
        int n = s.nextInt();
        int a[] = new int[n];
        System.out.println("Enter elements of array:");
        for(int i = 0; i < n; i++)
        {
            a[i] = s.nextInt();
        }
        Arrays.sort(a);
        System.out.println(a[1] + " is the Second smallest number");
		}

	}
