package week1.day2;

import java.util.Scanner;

public class usingRecurFactorial {

			static int factorial(int n){    
			  if (n == 0)    
			    return 1;    
			  else    
			    return(n * factorial(n-1));    
			 }    
			 public static void main(String args[]){  
			  int i,fact=1;  
			  System.out.println("Enter a number to find its factorial: ");
			  Scanner scr = new Scanner(System.in);
			  int num = scr.nextInt();
				  
			  fact = factorial(num);   
			  System.out.println("Factorial of "+num+" is: "+fact);    
			 }    

	}

