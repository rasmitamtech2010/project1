package week1.day2;

import java.util.Scanner;

public class FindDaysOfMonth {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
			
			String monthName=null;
			int days=0; 

				Scanner scr=new Scanner(System.in);
				System.out.print("Enter a Number: ");
		        		int month = scr.nextInt();

		        		System.out.print("Enter a year: ");
		        		int year=scr.nextInt();
		        
		        switch (month) {
		            	case 1:
		            		monthName="January";
		            		days=31;
		            		System.out.println("The "+ month +"st month of the Year is:"+monthName);
		            		System.out.print( "Number of days in the month of "+monthName+ " is " +days);
		            		break;
		            	case 2:
		            		monthName="February";
		            		if((year%400==0)||((year%4==0))){
		            			if(year%100!=0) {
		            				days=29;
		            			}
		            		}
		            		else
		            			days=28;
		            		System.out.println("The "+ month +"nd month of the Year is:"+monthName);
		            		System.out.print( "Number of days in the month of "+monthName+ " is " +days);
		            		break;
		            	case 3:
		            		monthName="March";
		            		days=31;
		            		System.out.println("The "+ month +"rd month of the Year is:"+monthName);
		            		System.out.print( "Number of days in the month of "+monthName+ " is " +days);
		            		break;
		            	case 4:
		            		monthName="April";
		            		days=30;
		            		System.out.println("The "+ month +"th month of the Year is:"+monthName);
		            		System.out.print( "Number of days in the month of "+monthName+ " is " +days);
		            		break;
		            	case 5:
		            		monthName="May";
		            		days=31;
		            		System.out.println("The "+ month +"th month of the Year is:"+monthName);
		            		System.out.print( "Number of days in the month of "+monthName+ " is " +days);
		            		break;
		            	case 6:
		            		monthName="June";
		            		days=30;
		            		System.out.println("The "+ month +"th month of the Year is:"+monthName);
		            		System.out.print( "Number of days in the month of "+monthName+ " is " +days);
		            		break;
		            	case 7:
		            		monthName="July";
		            		days=31;
		            		System.out.println("The "+ month +"th month of the Year is:"+monthName);
		            		System.out.print( "Number of days in the month of "+monthName+ " is " +days);
		            		break;
		            	case 8:
		            		monthName="August";
		            		days=31;
		            		System.out.println("The "+ month +"th month of the Year is:"+monthName);
		            		System.out.print( "Number of days in the month of "+monthName+ " is " +days);
		            		break;
		            	case 9:
		            		monthName="September";
		            		days=30;
		            		System.out.println("The "+ month +"th month of the Year is:"+monthName);
		            		System.out.print( "Number of days in the month of "+monthName+ " is " +days);
		            		break;
		            	case 10:
		            		monthName="October";
		            		days=31;
		            		System.out.println("The "+ month +"th month of the Year is:"+monthName);
		            		System.out.print( "Number of days in the month of "+monthName+ " is " +days);
		            		break;
		            	case 11:
		            		monthName="November";
		            		days=30;
		            		System.out.println("The "+ month +"th month of the Year is:"+monthName);
		            		System.out.print( "Number of days in the month of "+monthName+ " is " +days);
		            		break;
		            	case 12:
		            		monthName="December";
		            		days=31;
		            		System.out.println("The "+ month +"th month of the Year is:"+monthName);
		            		System.out.print( "Number of days in the month of "+monthName+ " is " +days);
		            		break;
		            	default:
		            		System.out.println("You have entered an invalid number");
		            		break;
			}
		}
 }

