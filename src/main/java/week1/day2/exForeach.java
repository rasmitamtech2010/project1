package week1.day2;

import java.util.Scanner;

public class exForeach {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.out.println("Enter Three models of mobile: ");
		String m[]=new String[3];
		Scanner scr=new Scanner(System.in);
		int size=m.length;		
		for(int i=0;i<size;i++) {
			m[i]=scr.next();
		}
		
		System.out.println("Three models of mobile are : ");
		for(String eachMob:m)
		{
			System.out.println(eachMob);
		}

		
	}

}
