package week1.day2;

import java.util.Scanner;

public class findFactorial {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int num, n, fact = 1;
		 
	      System.out.println("Enter a number to find its factorial: ");
	      Scanner scr = new Scanner(System.in);
	 
	      num = scr.nextInt();
	 
	      if (num < 0)
	         System.out.println("Number is negative.");
	      else
	      {
	         for (n = 1; n <= num; n++){
	          fact = fact*n;
	        }
	         System.out.println("The Factorial of "+num+" is :  "+fact);
	      }
	 
	}

}
