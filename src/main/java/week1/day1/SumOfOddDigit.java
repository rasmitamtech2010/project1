package week1.day1;

import java.util.Scanner;

public class SumOfOddDigit {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scr = new Scanner(System.in);
		System.out.print("Number: ");
		int number = scr.nextInt();
		int sum= 0;
		while (number > 0) {
		    int digit = number % 10;
		    if (digit % 2 != 0) {
			sum = sum + digit;
		    }
		    number = number / 10;
		}
		System.out.println(sum);   
	}

}
