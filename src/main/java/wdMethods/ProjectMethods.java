package wdMethods;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import utils.ReadExcel;



public class ProjectMethods extends SeMethods{
	@BeforeSuite(groups="common")
	public void beforeSuite() {
		
		beginResult();
	}
	@DataProvider(name="positive")
	public Object[][] fetch() throws IOException
	{
		
		return ReadExcel.readExcelData();
	}
	@BeforeClass(groups="common")
	public void beforeClass() {
		startTestCase();
	}
	
	@Parameters({"url"})
	@BeforeMethod(groups="common")
	public void login(String url) throws InterruptedException, IOException {
		startApp("chrome", url);
		/*WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, username);
		WebElement elePassword = locateElement("id","password");
		type(elePassword, password);
		WebElement eleLogin = locateElement("classname", "decorativeSubmit");
		click(eleLogin);
		Thread.sleep(3000);
		WebElement eleCRM = locateElement("linktext","CRM/SFA");
		click(eleCRM);*/
	}
	@AfterMethod(groups="common")
	public void closeApp() {
		closeBrowser();
	}
	@AfterSuite(groups="common")
	public void afterSuite() {
		endResult();
	}
	
	
}
