package week5.day1;

import java.io.IOException;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class LearnHtmlReport {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		ExtentHtmlReporter html=new ExtentHtmlReporter("./reports/result.html");
		html.setAppendExisting(true);
		ExtentReports extent=new ExtentReports();
		
		extent.attachReporter(html);
		//send information of your test case
		ExtentTest test=extent.createTest("TC002_CreateLead","Create a new Lead in leafTaps");
		test.assignCategory("Smoke");
		test.assignAuthor("Setu");
		//Generate Report for each step
		test.pass("The Browswer launched successfully",MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img1.png").build());
		test.pass("The data DemoSalesManager entered successfully",MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img2.png").build());
		test.pass("The Data crmsfa entered successfully",MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img3.png").build());
		test.pass("The Element Login Button Clicked Successfully",MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img4.png").build());
		extent.flush();
	}

}
