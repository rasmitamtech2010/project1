package testng;

import org.testng.annotations.Test;

public class TestNG2 {
	//@Test(priority = 3)
	@Test(enabled = false)
	public void createLead() {
		System.out.println("createLead");
		throw new RuntimeException();
	}
	@Test//(dependsOnMethods = {"createLead"})
	public void editLead() {
		System.out.println("editLead");
	}						
	//@Test(dependsOnMethods = {"testng.TestNG2.createLead"},alwaysRun = true)
	@Test(dependsOnMethods = {"testng.TestNG2.createLead"}/*,alwaysRun = true*/)
	public void deleteLead() {
		System.out.println("deleteLead");
	}	
	@Test
	public void mergeLead() {
		System.out.println("mergeLead");
	}
}