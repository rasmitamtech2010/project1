package POM_pages;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class FindLeadsPage extends ProjectMethods
{
public FindLeadsPage()
{
	PageFactory.initElements(driver, this);
}


@FindBy(how= How.XPATH,using="(//input[@name='firstName'])[3]")
WebElement elementLeadName;
public FindLeadsPage enterUserName(String data) throws IOException
{
	type(elementLeadName, data);
	return this;
}

@FindBy(how= How.XPATH,using="//button[text()='Find Leads']")
WebElement buttonFindLead;
public FindLeadsPage clickFindLeadButton() throws InterruptedException 
{
	click(buttonFindLead);
	Thread.sleep(7000);
	return this;
	
}


@FindBy(how= How.XPATH,using="//td[@class='x-grid3-col x-grid3-cell x-grid3-td-partyId x-grid3-cell-first ']//a")
WebElement elementFoundLead;
public ViewLeadPage clickFoundLeadLink()
{
	click(elementFoundLead);
	return new ViewLeadPage();
	
}

String foundLeadId;
public FindLeadsPage getFoundLeadTD()
{
	foundLeadId=getText(elementFoundLead);
	return this;
}
@FindBy(how= How.XPATH,using="//span[text()='Phone']")
WebElement elementPhone;
public FindLeadsPage clickPhoneTab()
{
	click(elementPhone);
	return this;
	
}


@FindBy(how= How.XPATH,using="//div[@class='x-paging-info']")
WebElement noRecordDisplayLabel;
public FindLeadsPage verifyNoRecordFound()
{
	verifyExactText(noRecordDisplayLabel, "No records to display");
	return this;
	
}


@FindBy(how= How.NAME,using="phoneCountryCode")
WebElement elementCountryCode;
public FindLeadsPage enterCountryCode(String data) throws IOException
{
	elementCountryCode.clear();
	type(elementCountryCode, data);
	return this;
	
}


@FindBy(how= How.XPATH,using="//span[text()='Email']")
WebElement tabEmail;
public FindLeadsPage clickEmailTab()
{
	click(tabEmail);
	return this;
	
}

@FindBy(how= How.NAME,using="emailAddress")
WebElement elementEmail;
public FindLeadsPage enterEmail(String data) throws IOException
{
	type(elementEmail, data);
	return this;
	
}


@FindBy(how= How.NAME,using="phoneNumber")
WebElement elementPhoneNumber;
public FindLeadsPage enterPhoneNumber(String data) throws IOException
{
	type(elementPhoneNumber, data);
	return this;
	
}

@FindBy(how= How.NAME,using="id")
WebElement elementLeadId;
public FindLeadsPage seachByLeadId() throws InterruptedException, IOException
{
	type(elementLeadId, foundLeadId);
	return clickFindLeadButton();
	
}
}