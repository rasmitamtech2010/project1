package POM_pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;


public class HomePage extends ProjectMethods
{
public HomePage()
{
	PageFactory.initElements(driver, this);
}
@FindBy(how= How.LINK_TEXT,using="CRM/SFA")
WebElement elementLink;
public MyHomePage clickCrmSfaLink()
{
	click(elementLink);
	return new MyHomePage();
}

@FindBy(how=How.CLASS_NAME,using="decorativeSubmit")
WebElement elementSubmit;
public LoginPage clickLogout() {
click(elementSubmit);
return new LoginPage();
}
}