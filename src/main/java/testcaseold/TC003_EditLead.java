package testcase;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import wdMethodsold.SeMethods;
	
	public class TC003_EditLead extends SeMethods {
		
		@Test
		public void Login() throws InterruptedException {
		startApp("chrome", "http://leaftaps.com/opentaps/control/login");
		WebElement eleUserName = locateElement("username");
		type(eleUserName, "DemoSalesManager");
		WebElement elePassword = locateElement("password");
		type(elePassword, "crmsfa");
		WebElement Login = locateElement("class","decorativeSubmit");
		click(Login);
		//WebElement Logout = locateElement("class","decorativeSubmit");
		WebElement Crmsfa = locateElement("linktext","CRM/SFA");
		click(Crmsfa);
		WebElement Lead = locateElement("linktext","Leads");
		click(Lead);
		Thread.sleep(3000);
		//Click Find Leads
		WebElement FindLead = locateElement("linktext","Find Leads");
		click(FindLead);
		//Enter First Name
		WebElement FirstName = locateElement("xpath","(//input[@name=\"firstName\"])[3]");
		type(FirstName, "Rasmita");
		//Click on Find Leads button
		WebElement FindLeads = locateElement("xpath","//button[contains(text(),'Find')]");
		click(FindLeads);
		
		
		}
}
