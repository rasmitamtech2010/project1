package testcase;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;
import wdMethodsold.SeMethods;

public class TC002_CreateLead extends ProjectMethods {
	@BeforeTest
	public void setData() {
		testCaseName="TC002_CreateLead";
		testCaseName="Create a new Lead";
		category="smoke";
		author="sethu";
	}
	
	@Test
	/*public void Login() throws InterruptedException {
	startApp("chrome", "http://leaftaps.com/opentaps/control/login");
	WebElement eleUserName = locateElement("username");
	type(eleUserName, "DemoSalesManager");
	WebElement elePassword = locateElement("password");
	type(elePassword, "crmsfa");
	WebElement Login = locateElement("class","decorativeSubmit");
	click(Login);
	//WebElement Logout = locateElement("class","decorativeSubmit");
	WebElement Crmsfa = locateElement("linktext","CRM/SFA");
	click(Crmsfa);*/
	
	public void createLead() {
	WebElement Lead = locateElement("linktext","Leads");
	click(Lead);
	//Thread.sleep(3000);
	WebElement CreateLead = locateElement("linktext","Create Lead");
	click(CreateLead);
	WebElement company = locateElement("createLeadForm_companyName");
	type(company, "Infosys");
	
	WebElement FirstName = locateElement("createLeadForm_firstName");
	type(FirstName, "Rasmita");
	WebElement LastName = locateElement("createLeadForm_lastName");
	type(LastName, "Satapathy");
	WebElement FrstLocName = locateElement("createLeadForm_firstNameLocal");
	type(FrstLocName, "Aditi");
	WebElement LastLocName = locateElement("createLeadForm_lastNameLocal");
	type(LastLocName, "S");
	WebElement PersonalTittle = locateElement("createLeadForm_personalTitle");
	type(PersonalTittle, "Welcome!!!");
	WebElement DatasrcId = locateElement("createLeadForm_dataSourceId");
	selectDropDownUsingText(DatasrcId,"Direct Mail");
	WebElement ProfTittle = locateElement("createLeadForm_generalProfTitle");
	type(ProfTittle , "Selenium");
	WebElement Revenue = locateElement("createLeadForm_annualRevenue");
	type(Revenue, "3,00,000");
	WebElement industryEnumId = locateElement("createLeadForm_industryEnumId");
	selectDropDownUsingText(industryEnumId,"Computer Software");
	WebElement OwnerShip = locateElement("createLeadForm_ownershipEnumId");
	selectDropDownUsingText(OwnerShip,"Public Corporation");
	WebElement SicCode = locateElement("createLeadForm_sicCode");
	type(SicCode,"94");
	WebElement Desc = locateElement("createLeadForm_description");
	type(Desc,"createLeadForm_description");
	WebElement ImpNote = locateElement("createLeadForm_importantNote");
	type(ImpNote,"createLeadForm_importantNote");
	WebElement CountryCode = locateElement("createLeadForm_primaryPhoneCountryCode");
	type(CountryCode,"IN");
	
	WebElement AreaCode = locateElement("createLeadForm_primaryPhoneAreaCode");
	type(AreaCode,"600061");
	
	WebElement PriPhone = locateElement("createLeadForm_primaryPhoneNumber");
	PriPhone.clear();
	type(PriPhone,"9439377221");

	WebElement Submit = locateElement("xpath","//input[@class='smallSubmit']");
	click(Submit);
	
	
	closeBrowser();
	}
		

	
	

}



