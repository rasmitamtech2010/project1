package wdMethodsold;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;


public class SeMethods implements WdMethods{
	public RemoteWebDriver driver;
	public int i = 1;
	public void startApp(String browser, String url) {
		if(browser.equalsIgnoreCase("chrome")) {
			System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
			driver  = new ChromeDriver();
		} else if(browser.equalsIgnoreCase("firefox")) {
			System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver.exe");
			driver  = new FirefoxDriver();
		}
		driver.get(url);
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		System.out.println("The Browser "+browser+" launched successfully");
		takeSnap();

	}


	public WebElement locateElement(String locator, String locValue) {
		switch (locator) {
		case "id": return driver.findElementById(locValue);
		case "name": return driver.findElementByName(locValue);
		case "tagname":return driver.findElementByTagName(locValue);
		case "class": return driver.findElementByClassName(locValue);
		case "xpath": return driver.findElementByXPath(locValue);
		case "linktext": return driver.findElementByLinkText(locValue);
		case "partiallinktext":return driver.findElementByPartialLinkText(locValue);
		}
		return null;
	}

	
	public WebElement locateElement(String locValue) {		
		return driver.findElementById(locValue);
	}

	public void type(WebElement ele, String data) {
		ele.sendKeys(data);
		System.out.println("The Data "+data+" is entered Successfully");
		takeSnap();
	}


	public void click(WebElement ele) {
		ele.click();
		System.out.println("The Element "+ele+" is clicked Successfully");
		takeSnap();
	}

	
	public String getText(WebElement ele) {
		String text = ele.getText();
		return text;
		//return null;
	}

	@Override
	public void selectDropDownUsingText(WebElement ele, String value) {
		Select slct=new Select(ele);
		slct.selectByVisibleText(value);
		System.out.println("The value "+value+"is present in Drop down");
	}

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {
		Select slct=new Select(ele);
		slct.selectByIndex(index);
		System.out.println("The index "+index+"is present in Drop down");

	}

	@Override
	public boolean verifyTitle(String expectedTitle) {
		boolean Breturn=false;
		String Tittle=driver.getTitle();
		if(expectedTitle.equals(Tittle))
		{
			System.out.println("Expected Tittle is matched the Actual Tittle");
			Breturn=true;
		}
		
		return Breturn;
	}

	@Override
	public void verifyExactText(WebElement ele, String expectedText) {
		
		String text=ele.getText();
		if(text.equals(expectedText))
		{
			System.out.println("Text Verified");
		}
	}

	@Override
	public void verifyPartialText(WebElement ele, String expectedText) {
		String text=ele.getText();
		if(text.contains(expectedText))
		{
			System.out.println("Parial Text Verified");
		}
	}

	@Override
	public void verifyExactAttribute(WebElement ele, String attribute, String value) {
		String LocValue=ele.getAttribute(attribute);
		if(LocValue.equals(value))
		{
			System.out.println("Exact Attribute value matched");
		}

	}

	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		String LocValue=ele.getAttribute(attribute);
		if(LocValue.contains(value))
		{
			System.out.println("Partial Attribute  value matched");
		}


	}

	@Override
	public void verifySelected(WebElement ele) {
		boolean selected = ele.isSelected();
		if(selected==true)
		{
			System.out.println("The element "+ele+" is already select" );
		}
		System.out.println("The element "+ele+" is not select" );
	}

	
	public void verifyDisplayed(WebElement ele) {
		
		boolean displayed = ele.isDisplayed();
		if(displayed==true)
		{
			System.out.println("The element "+ele+" is displayed");
		}
		else
			System.out.println("The element "+ele+" is not displayed");
	}
	

	
	public void switchToWindow(int index) {
		Set<String> windowHandles = driver.getWindowHandles();
		List<String> lst=new ArrayList<>();
		lst.addAll(windowHandles);
		driver.switchTo().window(lst.get(index));
		System.out.println("The window is switched to "+index);

	}

	
	public void switchToFrame(WebElement ele) {
		driver.switchTo().frame(ele);
		System.out.println("The controlled is switched to frame");
	}

	
	public void acceptAlert() {
		driver.switchTo().alert().accept();
		System.out.println("The alert is accepted");

	}

	@Override
	public void dismissAlert() {
		driver.switchTo().alert().dismiss();
		System.out.println("The alert is Rejected");

	}

	
	public String getAlertText() {
		Alert alert = driver.switchTo().alert();
		String text = alert.getText();
		System.out.println("The alert text is "+"\n"+text);
		return text;
	}


	public void takeSnap() {
		try {
		File src = driver.getScreenshotAs(OutputType.FILE);
		File desc = new File("./snaps/img"+i+".png");		
		FileUtils.copyFile(src, desc);
		} catch (IOException e) {
			e.printStackTrace();
		}
		i++;
	}

	@Override
	public void closeBrowser() {
		driver.close();
		System.out.println("The window closed successfully");
	}

	@Override
	public void closeAllBrowsers() {
		driver.quit();
		System.out.println("The Browser closed successfully");

	}

}